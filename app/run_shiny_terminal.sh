#!/bin/bash
TAG=shiny
IMAGENAME=cspinc/rfvs:$TAG
PASSWORD=CSP4Lyfe
LOCALDRIVE=/datadrive
PORT=3838

docker run --rm \
	-it \
	--name rfvs_shiny \
	-v $(pwd):/shiny \
	-w /shiny \
	-v $LOCALDRIVE:/data \
	-p 3838:3838 \
	-e PASSWORD=$PASSWORD \
	-e ROOT=TRUE \
	$IMAGENAME \
	/bin/bash
