#!/bin/bash
IMAGENAME=cspinc/rfvs:rstudio
#IMAGENAME=rfvs:last_hope
USERNAME=rstudio
PASSWORD=CSP4Lyfe
LOCALDRIVE=/datadrive

docker run -d --rm \
	--name rfvs \
	-v $(pwd):/home/rstudio/rfvs \
	-v $LOCALDRIVE:/data \
	-p 8686:8787 \
	-e PASSWORD=$PASSWORD \
	-e ROOT=TRUE \
	$IMAGENAME
