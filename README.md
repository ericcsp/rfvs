# Introduction
This is a "fork" of the `open-fvs` library found at https://sourceforge.net/projects/open-fvs/. CSP built this repository and docker image on July 15, 2020. The `open-fvs` project is ongoing development and will likely be updated in the near future. Please reference: https://sourceforge.net/p/open-fvs/wiki/rFVS/ for the wiki on using our docker image with R built on the `rocker/rstudio` image. 

To get a working rstudio version of rfvs:
1. Execute ./build\_rstudio\_image.sh
2. Execute run\_rstudio.sh
3. Once inside the rstudio session, source fvs\_init.R and execute the first 2 lines to load the system, and the third line to load the FVSiec data.
