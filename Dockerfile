FROM ubuntu:20.04

LABEL org.label-schema.license="GPL-2.0" \
      org.label-schema.vcs-url="https://gitlab.com/ericcsp" \
      org.label-schema.vendor="Conservation Science Partners" \
      maintainer="Eric Stofferahn <eric@csp-inc.org>"

RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    gfortran \
    cmake \
    unixodbc-dev \
    build-essential \
    subversion \
    r-base \
  && apt-get remove --purge -y $BUILDDEPS \
  && apt-get autoremove -y \
  && apt-get autoclean -y \
  && rm -rf /var/lib/apt/lists/*

RUN cd /usr/local/lib \
  && svn checkout https://svn.code.sf.net/p/open-fvs/code open-fvs \
  || cd open-fvs \
  && svn cleanup \
  && svn up \
  || svn cleanup \
  && svn up \
  || svn cleanup \
  && svn up

RUN cd /usr/local/lib/open-fvs/trunk/bin \
  && cmake -G"Unix Makefiles" . \
  && make

CMD /bin/bash
