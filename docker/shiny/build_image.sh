#!/bin/bash
TAG=shiny
IMAGENAME=cspinc/rfvs:$TAG
docker build --no-cache -t $IMAGENAME -f ./Dockerfile .
