#!/bin/bash
IMAGENAME=cspinc/rfvs:rstudio
LOCALDRIVE=/datadrive

docker run --rm \
	-it \
	--name=rfvs \
	-v $(pwd):/contents \
	-v $LOCALDRIVE:/data \
	$IMAGENAME \
	/bin/bash
